import bpy 
from shapely.geometry import MultiPolygon
from .geometry import *
from mathutils import  Vector

def avoid_linked_masks(gpm):
    if gpm.src_ob.library:
        src_key = gpm.src_ob.name
        gpm.src_ob = bpy.data.objects[src_key]

def mode_update(self,context) :
    if self.skip_mode_update : 
        return 

    mask_layer = get_mask_layer(self.id_data, self )
    if self.mode =='ON' :
        avoid_linked_masks(self)
        for id, gpm in enumerate(self.id_data.gpmdatas) :
            if gpm == self :
                if mask_layer :
                    clear_layer_anim(mask_layer)
                generate(self.id_data, id, [])
                mask_layer = get_mask_layer(self.id_data, self)
                mask_layer.hide = False
                break
    if self.mode == 'OFF' :
        if mask_layer :
            clear_layer_anim(mask_layer)
            mask_layer.hide = True

    if self.mode == 'BAKED' :
        cur_fr = context.scene.frame_current
        bake_mask(self, context.scene.frame_start, context.scene.frame_end)
        mask_layer = get_mask_layer(self.id_data, self)
        mask_layer.hide = False
        context.scene.frame_set(cur_fr)
  
    return None

def clear_layer_anim(layer):
    gp = layer.id_data
    if gp and gp.animation_data and gp.animation_data.action :
        for fc in reversed(gp.animation_data.action.fcurves) :
            if 'layers["%s"]' % layer.info in fc.data_path  :
                 gp.animation_data.action.fcurves.remove(fc)
          
def is_valid_gpm(gpm):
    if gpm.src_ob and len(gpm.layers) > 0 :
        return True
    else :
        return
def update_result_layer_info(self, context):
    #print(self.result_layer_info)
    result_layer = get_mask_layer(self.id_data, self)
    result_layer.info = self.result_layer_info
    self.result_layer = str(result_layer)
    #print(result_layer)
    return

def get_mask_layer(ob, gpm):
    ##print('looking for', gpm.result_layer)
    result_layer = [layer for layer in ob.data.layers if str(layer)[-19:-1] == gpm.result_layer[-19:-1]]
    if len(result_layer) == 1 :
        #print('found by memory location', result_layer[0])
        if str(result_layer[0])[:-19] != gpm.result_layer[:-19] : #name changed
            gpm.result_layer = str(result_layer[0])
        return result_layer[0]
    else :
        result_layer = [layer for layer in ob.data.layers if str(layer)[:-19] == gpm.result_layer[:-19]]
        if len(result_layer) == 1 :
            #print('found by name', result_layer[0])
            try :
                gpm.result_layer = str(result_layer[0])
            except :
                pass
            return result_layer[0]
        else :
            #print('not found')
            return None

def get_active_frame(layer, frame_number):
    for i, frame in enumerate(layer.frames):
        if frame.frame_number == frame_number :
            return frame
        if frame.frame_number > frame_number :
            if i > 0:
                return layer.frames[i-1]
            else :
                return None
    return layer.frames[len(layer.frames) - 1]

def bake_mask(gpm, start, end):
    if is_valid_gpm(gpm):
        dest_ob = gpm.id_data
        mask_layer = get_mask_layer(dest_ob, gpm )
        if mask_layer : 
            mask_layer.clear()
        for id, gpmd in enumerate(dest_ob.gpmdatas) :
            if gpmd == gpm :
                generate(dest_ob, id, [i for i in range(start, end+1)])
                break 
        mask_layer = get_mask_layer(dest_ob, gpm )
        print('BAKE ', mask_layer.info, dest_ob.name)
        #keyframe layer position
        for frame_nb in reversed(range(start, end+1)) :
            bpy.context.scene.frame_set(frame_nb)
            refresh_masklayer_transforms([gpm])
            dest_ob.data.keyframe_insert(data_path = 'layers["'+mask_layer.info+'"].location', index = -1)
            dest_ob.data.keyframe_insert(data_path = 'layers["'+mask_layer.info+'"].rotation', index = -1)
            dest_ob.data.keyframe_insert(data_path = 'layers["'+mask_layer.info+'"].scale', index = -1)

    #unobserve users > don't redraw anymore
    

    return

def bake_all_masks(mask_list, start, end):
    scene = bpy.context.scene
    frame_current = scene.frame_current

    mask_layer_list = []
    for mask in mask_list : 
        dest_ob = mask.id_data

        mask_layer = get_mask_layer(dest_ob, mask)
        if mask_layer : 
            mask_layer.clear()

        for id, gpmd in enumerate(dest_ob.gpmdatas) :
            if gpmd == mask :
                generate(dest_ob, id, [i for i in range(start, end+1)])
                break 
        
        mask_layer = get_mask_layer(dest_ob, mask)
        mask_layer_list.append([mask, mask_layer])


    for frame_nb in reversed(range(start, end+1)) :
        scene.frame_set(frame_nb)

        for mask, mask_layer in mask_layer_list :
            dest_ob = mask.id_data
            refresh_masklayer_transforms([mask])
            dest_ob.data.keyframe_insert(data_path = 'layers["'+mask_layer.info+'"].location', index = -1)
            dest_ob.data.keyframe_insert(data_path = 'layers["'+mask_layer.info+'"].rotation', index = -1)
            dest_ob.data.keyframe_insert(data_path = 'layers["'+mask_layer.info+'"].scale', index = -1)
        
    scene.frame_set(frame_current)

def calculate_frame(gpm, mask_layer, frame_number, mask_material_index):
    src_ob = gpm.src_ob
    #check for existing frame at current number
    for frame in mask_layer.frames :
        if frame.frame_number == frame_number:
            mask_layer.frames.remove(frame)
    
    result_frame = mask_layer.frames.new(frame_number, active = False)
    result = None
    for i, gpmlayer in enumerate(gpm.layers) :
        src_layer = [layer for layer in src_ob.data.layers if layer.info == gpmlayer.info][0] 
        src_frame = get_active_frame(src_layer, frame_number)

        src_shape = convert_frame_to_shapes(src_frame, src_ob)
        
        if i == 0 :
            result = src_shape
        if i > 0 :
            if gpmlayer.mix_mode == 'ADD':
                result = result.union(src_shape)
            if gpmlayer.mix_mode == 'SUBSTRACT':
                result = result.difference(src_shape)
            if gpmlayer.mix_mode == 'INTERSECTION':
                result = result.intersection(src_shape)
            if gpmlayer.mix_mode == 'SYM_DIFF':
                result = result.symmetric_difference(src_shape)
    
    if result is not None :
        if result.geom_type == 'Polygon' :
            result = MultiPolygon([result])
        no_int_shapes = split_interiors(result)
        draw_shapes(no_int_shapes, result_frame, mask_material_index)
        return result_frame
    else :
        return None

def get_mask_material_index(ob) :
    already_here = False
    for i, material in enumerate(ob.data.materials) :
        if material and material.name == 'GPMAT_mask_material' :
            already_here = True
            return i
    if already_here == False :
        mat_in_data = False
        for material in bpy.data.materials :
            if material.name == 'GPMAT_mask_material' :
                mat_in_data = True  
                ob.data.materials.append(material) 
                return len(ob.data.materials) - 1  
        if mat_in_data == False :    
            material = bpy.data.materials.new('GPMAT_mask_material')
            bpy.data.materials.create_gpencil_data(material)
            material.grease_pencil.show_stroke = False
            material.grease_pencil.show_fill = True
            material.grease_pencil.fill_style = 'SOLID'
            material.grease_pencil.fill_color = (0.0, 1.0, 0.0, 1.0)
            material.grease_pencil.use_fill_holdout = False
            ob.data.materials.append(material) 
            return len(ob.data.materials) - 1

def remove_mask_layer(ob, i):
    gpm = ob.gpmdatas[i]
    result_layer = get_mask_layer(ob, gpm)
    if result_layer is not None :
        ob.data.layers.remove(result_layer)
        gpm.result_layer = 'None'
    return None

def refresh_masklayer_transforms(gpm_users):
    #print('refresh')
    for gpm in gpm_users :
        dest_ob = gpm.id_data
        src_ob = gpm.src_ob
        #print(dest_ob.name,'___', src_ob.name)
        if dest_ob and src_ob :
            dest_ob.update_tag()
            src_ob.update_tag()
            result_layer = get_mask_layer(dest_ob, gpm) #dest_ob.data.layers[gpm.result_layer.split('"')[1]] 
            result_mat = compute_matrix(dest_ob, src_ob)
            if result_mat :
                result_mat = result_mat.decompose()
            #print(result_layer.info, result_mat)
            #result_layer.matrix_layer = result_mat
                result_layer.location  = result_mat[0] + Vector((0, gpm.offset/bpy.context.scene.unit_settings.scale_length, 0))
                result_layer.rotation = result_mat[1].to_euler()
                result_layer.scale = result_mat[2]
    return     



def generate(ob, i, frames):
    gpm = ob.gpmdatas[i]
    src_ob = gpm.src_ob
    #print('generate : ', ob.name, ' ___ ', src_ob.name, frames)
    mask_layer = get_mask_layer(ob, gpm)
    if mask_layer == None :
            mask_layer = ob.data.layers.new('GPLYR_mask_' + str(i), set_active = False)
            mask_layer.opacity = 0.0
            mask_layer.use_onion_skinning = False
            mask_layer.use_lights = False
            mask_layer.lock = False
            gpm.result_layer = str(mask_layer)
            gpm.result_layer_info = mask_layer.info
    #mask_layer.hide = True
    #bpy.context.view_layer.update()
    mask_layer.hide = False

    #check for mask material
    mask_material_index = get_mask_material_index(ob)

    #compute operations between gpm.layers
    frame_numbers = []


        ## to fix : process tous les layers de l'objet, meme si pas utilisé dans le mask
    for layer in src_ob.data.layers :
        if layer.info in [layer.info for layer in gpm.layers] :
            if frames == []:
                for frame_number in [frame.frame_number for frame in layer.frames if frame.frame_number not in frame_numbers] :
                    frame_numbers.append(frame_number)
            else :
                for frame_number in [frame.frame_number for frame in layer.frames if frame.frame_number not in frame_numbers and frame.frame_number in frames] :
                    frame_numbers.append(frame_number)
            
    if frames == []:
        mask_layer.clear()
    
    #print('calc frames :', frame_numbers)
    for frame_number in frame_numbers :
        calculate_frame(gpm, mask_layer, frame_number, mask_material_index)
    
    refresh_masklayer_transforms([gpm])
    return None