# GP_Masks
Masquage entre objets Grease pencil. Génerer un calque issu d'un autre objet pour s'en servir de masque.

# [Download latest as zip](https://gitlab.com/andarta.pictures/blender/gp_masks/-/archive/main/gp_masks-main.zip)

# Installation

Download the repository as a .zip file, access it with blender through Edit/Preferences/Add-ons/Install, then activate it by checking the box in the add-on list.

# User guide (old)

![guide](docs/gpmask.png "guide")

# Bugs limitations and ideas
- you might have to allow scipt execution for driver function (make sure to allow permanently)
- drivers can be a bit slow to update mask position (moving current frame can help them to recalculate)
- non uniform scale combined to rotation can lead to some mask shearing, you should avoid that
- masks kind of works with line materials, but doesn't takes into account textures, thickness modifiers or thickness scale factor, better keep working with fills only

# Warning

This tool is in early development stage, thank you for giving it a try ! please report any bug or suggestion to t.viguier@andarta-pictures.com

# Visit us 

[https://www.andarta-pictures.com/](https://www.andarta-pictures.com/)


