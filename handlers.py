import bpy
from bpy.app.handlers import persistent
from .gp_utils import *
from mathutils import Matrix
import os

'''
observed_objects data structure :

{
    observed object :
                    [
                        observed_object Matrix world ,
                        [users gp masks] 
                    ]
}
'''

@persistent
def refresh_drivers_handler(self, context) :
    if not bpy.context.scene.gpm_scene.lock_handler :
        #print('h', bpy.context.scene.frame_current, len(bpy.context.scene.gpm_scene.observed_objects))
        gpms = []
        for ob in bpy.context.scene.gpm_scene.observed_objects :
            #print(ob.name)
            if str(ob) == '<bpy_struct, Object invalid>' :
                #print('rebuild list')
                rebuild_observed_lists(bpy.context, bpy.context.scene)
                refresh_drivers_handler(self, context)
                return
            if ob.matrix_world != bpy.context.scene.gpm_scene.observed_objects[ob][0] :
                #print('need to refresh ', ob.name, bpy.context.scene.gpm_scene.observed_objects[ob][1])
                if len(bpy.context.scene.gpm_scene.observed_objects[ob][1])>0 :
                    for gpm in bpy.context.scene.gpm_scene.observed_objects[ob][1] :
                        if gpm not in gpms and gpm.mode == 'ON':
                            gpms.append(gpm)
                else :
                    for gpm in ob.gpmdatas :
                        if gpm not in gpms and gpm.mode == 'ON' :
                            gpms.append(gpm)
                        
                        #print('---', gpm.src_ob.name)
                bpy.context.scene.gpm_scene.observed_objects[ob][0] = Matrix(ob.matrix_world)
        if len(gpms) > 0 :
            refresh_masklayer_transforms(gpms) 
        #print('-')
        return

@persistent
def refresh_drivers_render_handler(self, context) :
    print('mask render handler -', bpy.context.scene.frame_current)
    bpy.context.view_layer.update()
    gpms = []
    for ob in bpy.context.scene.objects :
        for gpm in ob.gpmdatas :
            if is_valid_gpm(gpm):
                gpms.append(gpm)

    if len(gpms) > 0 :
        refresh_masklayer_transforms(gpms) 
    
    return

def store_ob_and_time(context) :
    if bpy.context.object.type == 'GPENCIL' :
        if bpy.context.object.name not in context.scene.gpm_scene.non_objmode_journey.keys() :
            context.scene.gpm_scene.non_objmode_journey[bpy.context.object.name]= [context.scene.frame_current]
        else :
            if context.scene.frame_current not in context.scene.gpm_scene.non_objmode_journey[bpy.context.object.name]:
                context.scene.gpm_scene.non_objmode_journey[bpy.context.object.name].append(context.scene.frame_current)
@persistent
def user_mode_handler(self, context) :
    prev_mode = context.scene.gpm_scene.in_obj_mode
    if prev_mode == False and  bpy.context.mode == 'OBJECT':
        #print('just switched to object mode ')
        context.scene.gpm_scene.in_obj_mode = True

        print(context.scene.gpm_scene.non_objmode_journey)
        for key in context.scene.gpm_scene.non_objmode_journey :
            ob = bpy.data.objects[key]
            #print('----', ob.name)
            for i, gpm in enumerate(ob.gpmdatas) :
                if is_valid_gpm(gpm) :
                    generate(ob, i, context.scene.gpm_scene.non_objmode_journey[key])
            for layer in [layer for layer in ob.data.layers if layer in context.scene.gpm_scene.observed_layers.keys()] : 
                #print(layer.info)
                for user_ob, i in context.scene.gpm_scene.observed_layers[layer] :
                    if user_ob.gpmdatas[i].mode == 'ON' :
                        generate(user_ob, i, context.scene.gpm_scene.non_objmode_journey[key])
        context.scene.gpm_scene.non_objmode_journey.clear() 
                    
    if prev_mode == True and  bpy.context.mode != 'OBJECT':
        #print('just left object mode')
        context.scene.gpm_scene.in_obj_mode = False
        store_ob_and_time(context)
    if prev_mode == False and  bpy.context.mode != 'OBJECT':
        #print(context.scene.gpm_scene.non_objmode_journey)
        #print('staying in non object_mode')
        store_ob_and_time(context)
 
def rebuild_observed_lists(context, scene) :  
    #print('rebuild lists')
    scene.gpm_scene.observed_objects.clear()
    context.scene.gpm_scene.observed_layers.clear()
    for ob in scene.objects :
        #if ob.type == 'GPENCIL' and ob.gpmdatas :
        if ob.type == 'GPENCIL' and len(ob.gpmdatas) > 0 :
            observe_object(context,ob, None) 
            for i, gpm in enumerate(ob.gpmdatas):
                if is_valid_gpm(gpm):
                    for gpmlayer in gpm.layers :
                        observe_layer(context, gpmlayer, ob, i)     
                        observe_object(context, gpm.src_ob, gpm)

@persistent
def gpm_load_handler(self, context):
    print('GPM LOAD')
    rebuild_observed_lists(bpy.context, bpy.context.scene)    
    bpy.context.scene.gpm_scene.lock_handler = False
    return None

def observe_layer(context, gpmlayer, destinationobject, i):
    if not gpmlayer.info in gpmlayer.ob.data.layers.keys() :
        return
    layer = gpmlayer.ob.data.layers[gpmlayer.info]
    if layer not in context.scene.gpm_scene.observed_layers :
            context.scene.gpm_scene.observed_layers[layer] = []
    if destinationobject not in context.scene.gpm_scene.observed_layers[layer] :
        context.scene.gpm_scene.observed_layers[layer].append((destinationobject, i))

def unobserve_layer(context, gpmlayer, destinationobject, i):
    if gpmlayer.ob :
        if not gpmlayer.info in gpmlayer.ob.data.layers.keys() :
            return
        layer = gpmlayer.ob.data.layers[gpmlayer.info]
        if layer in context.scene.gpm_scene.observed_layers : #should be always true
            if len(context.scene.gpm_scene.observed_layers[layer]) > 1 :
                context.scene.gpm_scene.observed_layers[layer].remove((destinationobject, i))
            else :
                del context.scene.gpm_scene.observed_layers[layer]

def observe_object(context, ob, usergpm):
    if not usergpm :
        usergpm = []
    else : 
        usergpm = [usergpm]   
    if ob not in context.scene.gpm_scene.observed_objects.keys():
        context.scene.gpm_scene.observed_objects[ob] = [Matrix(ob.matrix_world), usergpm]
    else :
        if usergpm and usergpm not in context.scene.gpm_scene.observed_objects[ob][1]:
            context.scene.gpm_scene.observed_objects[ob][1].append(usergpm[0])

def unobserve_object(context, ob, usergpm):
    if ob in context.scene.gpm_scene.observed_objects.keys():
        if usergpm in context.scene.gpm_scene.observed_objects[ob][1]:
            context.scene.gpm_scene.observed_objects[ob][1].remove(usergpm)
        if len(context.scene.gpm_scene.observed_objects[ob][1]) == 0:
            del context.scene.gpm_scene.observed_objects[ob]