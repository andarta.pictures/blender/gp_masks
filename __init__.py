# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

bl_info = {
    "name" : "GP_Masks",
    "author" : "Tom VIGUIER",
    "description" : "import masks from a GP object to another ",
    "blender" : (3, 6, 0),
    "version" : (0, 1, 1),
    "location" : "Grease pencil properties, Viewport N-panel",
    "warning" : "EXPERIMENTAL TOOL",
    "category" : "Andarta"
}

import bpy
from bpy.types import PropertyGroup
from bpy.props import *
from .geometry import *
from .gp_utils import *
# from . import operators as ops
from .operators import *
from .ui import *
from .handlers import *
from .properties import *


from .import_utils import ensure_lib
optional_lib= [
    ['shapely','shapely'],
    ]
for lib in optional_lib :
    has_lib = ensure_lib(lib[0],lib[1])
    if not has_lib:
        print("Warning: Could not import nor install " + lib[0] +'\n'
              + 'Some features may not work properly \n' +              
                'Try again and if this error persist install dependencies manually')

classes = [ 
    GP_MASK_OT_MENU, 
    GP_MASK_OT_LAYER_ADD,
    GP_MASK_OT_LAYER_REMOVE,
    GP_MASK_OT_ADD, 
    GP_MASK_OT_REMOVE, 
    GP_MASK_OT_GENERATE,
    GP_MASK_OT_SWITCH_ON,
    GP_MASK_OT_SWITCH_OFF,
    GP_MASK_OT_SWITCH_ALL_ON,
    GP_MASK_OT_SWITCH_ALL_OFF,
    GP_MASK_OT_BAKE,
    GP_MASK_OT_BAKE_ALL,
    GP_MASK_RESTORE_DATA,

    GP_MASK_UL_LAYER_LIST, 
    GP_MASK_LAYER, 
    GP_MASK_PROPS, 
    GP_MASK_PT_PANEL,
    #GP_MASK_PT_3DV_PANEL,
    GP_MASK_PT_3DV_PANEL2,
    GP_MASK_WINPROPS,
    GP_MASK_SCENEPROPS,
    
]

def register():
    for cls in classes :
        bpy.utils.register_class(cls)  
    bpy.types.Object.gpmdatas = bpy.props.CollectionProperty(type = GP_MASK_PROPS )
    bpy.types.WindowManager.gpm_window = bpy.props.PointerProperty(type = GP_MASK_WINPROPS )
    bpy.types.Scene.gpm_scene = bpy.props.PointerProperty(type = GP_MASK_SCENEPROPS )
    bpy.app.handlers.depsgraph_update_post.append(user_mode_handler)
    bpy.app.handlers.load_post.append(gpm_load_handler)
    bpy.app.handlers.depsgraph_update_post.append(refresh_drivers_handler)
    bpy.app.handlers.frame_change_post.append(refresh_drivers_handler)
    bpy.app.handlers.render_pre.append(refresh_drivers_render_handler)
    
    
    #bpy.app.driver_namespace['drive_mask_layer'] = drive_mask_layer

def unregister():
    for cls in classes :
        bpy.utils.unregister_class(cls)
    del bpy.types.Object.gpmdatas 
    del bpy.types.WindowManager.gpm_window
    del bpy.types.Scene.gpm_scene
    bpy.app.handlers.depsgraph_update_post.remove(user_mode_handler)
    bpy.app.handlers.load_post.remove(gpm_load_handler)
    bpy.app.handlers.depsgraph_update_post.remove(refresh_drivers_handler)
    bpy.app.handlers.frame_change_post.remove(refresh_drivers_handler)
    bpy.app.handlers.render_pre.remove(refresh_drivers_render_handler)
    

    #del bpy.app.driver_namespace['drive_mask_layer']

if __name__ == "__main__":
    register()           
