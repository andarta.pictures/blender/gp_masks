import bpy
from mathutils import  Vector
from shapely.geometry import LineString, MultiPoint, Point, Polygon, MultiPolygon
from shapely.ops import split, unary_union
from mathutils import Matrix, Vector, Euler, Quaternion
import numpy as np

def divide_scale(sca1, sca2):
    return Vector((abs(sca1[0]/sca2[0]), abs(sca1[1]/sca2[1]), abs(sca1[2]/sca2[2])))
def substract_eulers(euler1, euler2):
    # Convert Euler angles to radians
    euler1 = Euler((euler1[0], euler1[1], euler1[2]), 'XYZ')
    euler2 = Euler((euler2[0], euler2[1], euler2[2]), 'XYZ')

    # Convert Euler angles to Quaternions
    quat1 = euler1.to_quaternion()
    quat2 = euler2.to_quaternion()

    # Calculate the rotation difference
    diff_quat = quat2.rotation_difference(quat1)

    # Convert the resulting Quaternion back to Euler
    diff_euler = diff_quat.to_euler('XYZ')
    return diff_euler

def compute_matrix(dest_ob, src_ob):
    '''Computes the delta between two objects,  
    applying result matrix to a layer in dest_ob will make it visually match src_ob'''

    scene = bpy.context.scene
    camera = scene.camera
    if camera :
        dest_mat = Matrix(dest_ob.matrix_world)
        dest_mat_inv = Matrix(dest_mat)
        dest_mat_inv.invert()
        src_mat = Matrix(src_ob.matrix_world)
    
        p0 = np.array(dest_mat.to_translation()) #destination object location
        v = Vector((0,1,0))
        v.rotate(dest_mat.to_euler()) 
        n = np.array(v) #orientation of destination object (orthogonal vector)
        l0 = np.array(Matrix(camera.matrix_world).to_translation()) #camera location
        l = np.array(src_mat.to_translation()) - l0 #src object location relative to camera (orentation vector)

        intersection = Vector(calculate_intersection(p0, n, l0, l))
        result_location = dest_mat_inv @ intersection
        result_rotation = substract_eulers(src_mat.to_euler(), dest_mat.to_euler())
        scale_ratio = Vector(intersection - Vector(l0)).length / Vector(l).length  # distance intersection<>cam / distance src_ob<>cam
        result_scale = divide_scale(src_mat.to_scale(), dest_mat.to_scale()) * scale_ratio
        #print(result_location, result_rotation, result_scale)
        result_mat = Matrix.LocRotScale(result_location, result_rotation, result_scale)
        return  result_mat
    return None
    
def calculate_intersection(p0, n, l0, l) :
    """Calculates the intersection between a line and a plane.
    
    - p0 : any point of the plane (np.array)
    - n : normal vector of the plane (np.array)
    - l0 : any point of the line (np.array)
    - l : direction vector of the line (np.array)
    
    Returns intersection as a 3D np.array"""

    d = np.dot(p0-l0, n)/np.dot(l, n)
    p = l0 + l*d
    #tolerance = 1e-1000
    #print(np.allclose(np.dot(p - p0, n), 0, atol=tolerance))
    return p
def split_interiors(multipolygon):
    
    result_list = []
    for polygon in multipolygon.geoms :
        if len(polygon.interiors) > 0:
            interior = polygon.interiors[0]
            minx, miny, maxx, maxy = interior.bounds     
            ##print('CUT!')
            result = split(polygon,LineString( [((minx + maxx)/2,-100000),((minx + maxx)/2,100000)] ))
            
            recursive_result = split_interiors(result)
            for geom in recursive_result.geoms :
                result_list.append(geom)

        else : result_list.append(polygon)
    ##print('splitted !', len(result_list))
    return MultiPolygon(result_list)
    
def co_3d_to_XZ(vector):
    return (vector[0], vector[2])

def co_XZ_to_3d(coord) :
    return Vector((coord[0], 0.0, coord[1]))


def draw_shapes(mutlipolygon, frame, fill_material_index):
    #print('-----DRAW')
    #start_time = time.time()
    for shape in mutlipolygon.geoms :

        result_stroke = frame.strokes.new()
        result_stroke.material_index = fill_material_index
        result_stroke.use_cyclic = True

        result_stroke.points.add(len(shape.exterior.coords[:-1]), pressure=10, strength=10)  
        for i, shape_point_co in  enumerate(shape.exterior.coords[:-1]):
                result_point = result_stroke.points[i]
                result_point.co = co_XZ_to_3d(shape_point_co)
        result_stroke.points.update()

    #print('DRAW TIME : ', time.time() - start_time)

def convert_frame_to_shapes(frame, ob):
    #start_time = time.time()
    ratio = 2000.0
    simplify_factor= 0.001
    mod_factor = 1
    for mod in ob.grease_pencil_modifiers :
        if mod.type == 'GP_THICK' :
            mod_factor *= mod.thickness_factor
    
    #print('-----CONVERT')
    shapes = []
    if frame is None :
        return MultiPolygon()
    for stroke in frame.strokes :
        stroke_material = stroke.id_data.materials[stroke.material_index]
        
        if len(stroke.points) < 4 :
            continue

        if stroke_material.grease_pencil.show_stroke == True : #add converted lines
            
            dots_list = []
            segments_list = []
            for point in stroke.points :
                shape_point = Point(co_3d_to_XZ(point.co))
                dot = shape_point.buffer(distance=  (point.pressure * stroke.line_width / ratio)*mod_factor , resolution=5, cap_style=1, join_style=1, mitre_limit=5.0, single_sided=False) #inclure
                dots_list.append(dot)

            for i, dot in enumerate(dots_list):
                if i > 0 :
                    dots_as_points = MultiPoint([*list(dot.exterior.coords), *list(dots_list[i-1].exterior.coords)])
                    segment = dots_as_points.convex_hull
                    segments_list.append(segment)

            result_shape = unary_union(segments_list)#.simplify(simplify_factor, preserve_topology=False)        
            shapes.append(result_shape)

        if stroke_material.grease_pencil.show_fill == True : #add fills
            points_list = []
            for point in stroke.points :
                points_list.append(co_3d_to_XZ(point.co))

            result_shape = Polygon(points_list)
            if result_shape.is_valid == False :      
                result_shape = Polygon(points_list).buffer(0)

            shapes.append(result_shape)

    if len(shapes) > 0 :    
        union_result = unary_union(shapes)
        ##print(union_result.geom_type)
        if union_result.geom_type == 'Polygon' :
            union_result = MultiPolygon([union_result])
        
        
        #print('CONVERT TIME : ', time.time() - start_time)
        return union_result
    else:
        return MultiPolygon()
