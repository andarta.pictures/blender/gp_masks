import bpy 
from bpy.types import UIList, Panel
from .gp_utils import *

def draw_panel(self, context):
    ob = context.active_object
    if ob.type == 'GPENCIL' :
        layout = self.layout
        for i, gpm in enumerate(ob.gpmdatas):
            row = layout.row()
            row.prop(gpm, "src_ob", text="GP Object") #should be able to filter GP objects only
            remove = row.operator("gpm.remove", text="X")
            remove.gpm = i
            remove.object_name = ob.name
            if gpm.src_ob and  gpm.src_ob.type == 'GPENCIL':
                row = layout.row()
                row.prop(gpm, "mode", text = 'Mode')
                row.prop(gpm, 'offset', text = 'offset y')
                if gpm.mode == 'ON':
                    row = layout.row()
                    generate = row.operator("gpm.generate", text = 'REFRESH')
                    generate.i = i
                    #generate.refresh = True      
                    generate.object_name = ob.name   
                row = layout.row()
                #row.prop(gpm, "invert")

                result_layer = get_mask_layer(ob, gpm)
                #result_layer = [ layer for layer in ob.data.layers if str(layer)[-19:-1] == gpm.result_layer[-19:-1]]
                if result_layer is not None :
                    #print(str(result_layer), gpm.result_layer)
                    #row.prop(result_layer, 'info', text = 'result layer :')
                    row.prop(gpm, 'result_layer_info', text = 'result layer :')
                
                
                row = layout.row()
                col = row.column()
                col.template_list("GP_MASK_UL_LAYER_LIST", "", gpm, "layers", gpm, "active_index", rows=4, sort_lock=True)
                
                col2 = row.column(align=True)   
                menu = col2.operator("gpm.menu", icon='ADD', text="")
                menu.i = i 

                remove = col2.operator("gpm.layerremove", icon='REMOVE', text="")
                remove.i = i

            elif gpm.src_ob and gpm.src_ob.type != 'GPENCIL' : 
                row = layout.row()
                row.label(text = 'Please pick a Grease Pencil Object !')
            '''elif gpm.src_ob and gpm.src_ob == ob : 
                row = layout.row()
                row.label(text = 'Please pick another Grease Pencil Object !')'''

        row = layout.row()
        row.operator("gpm.add", text="New")   

class GP_MASK_UL_LAYER_LIST(UIList):
    def draw_item(self, _context, layout, _data, item, icon, _active_data, _active_propname, _index):
        
        if self.layout_type in {'DEFAULT', 'COMPACT'}:
            row = layout.row(align=True)
            row.prop(item, "info", text="", emboss=False, icon_value=icon)
            #row.label(text = str(_index))
            if _index > 0 :
                row.prop(item, "mix_mode", text = '')
            #row.prop(mask, "invert", text="", emboss=False)
            #row.prop(mask, "hide", text="", emboss=False)
        elif self.layout_type == 'GRID':
            layout.alignment = 'CENTER'
            layout.prop(item, "info", text="", emboss=False, icon_value=icon)


class GP_MASK_PT_3DV_PANEL2(Panel):
    
    bl_label = "Scene"
    bl_idname = "GPMASK2_PT_PANEL"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    #bl_context = "scene"
    bl_category = 'Masks'

    def draw(self, context):
        layout = self.layout
        row = layout.row()
        row.label(text = 'ALL :')
        row.operator("gpm.switch_all_on", text = 'ON')
        row.operator("gpm.switch_all_off", text = 'OFF')
        row.operator("gpm.bake_all", text = 'BAKE')

        n = 0
        for object in [ob for ob in bpy.context.scene.objects if len(ob.gpmdatas) > 0]:
            
            row = layout.row()
            box = row.box()
            box.label(text = object.name + ' : ' )
            
            for i, gpm in enumerate(object.gpmdatas) :
                row = box.row()
                result_layer_name = ''
                if gpm.result_layer != '' :
                    result_layer = get_mask_layer(object, gpm)
                    if result_layer is not None :
                        result_layer_name = result_layer.info
                
                row.label(text = result_layer_name)
                if gpm.src_ob :
                    d_on = False
                    d_off = False
                    d_bake = False
                    if gpm.mode == 'ON':
                        d_on = True
                    elif gpm.mode == 'OFF':
                        d_off = True
                    elif gpm.mode == 'BAKED':
                        d_bake = True




                    turn_on = row.operator('gpm.switch_on', text = 'ON', depress = d_on)
                    turn_on.i = i
                    turn_on.object_name = object.name
                    turn_off = row.operator('gpm.switch_off', text = 'OFF', depress = d_off)
                    turn_off.i = i
                    turn_off.object_name = object.name
                    bake = row.operator('gpm.bake', text = 'BAKE', depress = d_bake )
                    bake.i = i
                    bake.object_name = object.name

                n += 1


        if n == 0 :
            row = layout.row()
            row.label(text= 'nothing to show')

class GP_MASK_PT_3DV_PANEL(bpy.types.Panel):
    
    bl_label = "Active Object"
    bl_idname = "GPMASK_PT_PANEL"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    #bl_context = "scene"
    bl_category = 'Masks'

    def draw(self, context):
        draw_panel(self, context)


class GPMDataButtonsPanel:
    bl_label = 'panel_filter'
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "data"

    @classmethod
    def poll(cls, context):
        return context.gpencil

class GP_MASK_PT_PANEL(GPMDataButtonsPanel, Panel):
    bl_label = "GP Advanced Masking"
    #bl_idname = "GPMASK_PT_layout"
    #bl_space_type = 'PROPERTIES'
    #bl_region_type = 'WINDOW'
    #bl_context = "data"
    
    def draw(self, context):
        draw_panel(self, context)



def draw_menu(self, context):
    i = bpy.context.window_manager.gpm_window.gpm_i
    layout = self.layout
    ob = context.object
    gpm = ob.gpmdatas[i]
    src_ob = gpm.src_ob
    gpd = src_ob.data
    gpl_active = gpd.layers.active   
    done = False

    for gpl in gpd.layers:
        if gpl : #filtrer pour éviter de mettre 2 fois le meme layer dans la liste
            done = True
            add = layout.operator("gpm.layeradd", text=gpl.info)
            add.info = gpl.info
            add.ob = src_ob.name

    if done is False:
        layout.label(text="No layers to add")
