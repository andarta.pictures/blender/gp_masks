
import bpy
from bpy.types import PropertyGroup
from .handlers import *



class GP_MASK_LAYER(PropertyGroup):
    bl_label = "gp mask layer"
    bl_idname = "gpmask_layer"
    
    ob : bpy.props.PointerProperty(type = bpy.types.Object)
    info : bpy.props.StringProperty()

    bool : bpy.props.BoolProperty(default = False)
    mix_mode : bpy.props.EnumProperty(items = [('ADD','Add', 'add',  0),('SUBSTRACT', 'Substract', 'substract',  1),('INTERSECTION', 'Intersection', 'intersection',  2),('SYM_DIFF', 'Symmetric Difference', 'symmetric difference', 3)], name = 'mix mode')

class GP_MASK_SCENEPROPS(PropertyGroup):
    bl_label = "gp mask scene properties"
    bl_idname = "gpmask_sceneproperties"
    lock_handler : bpy.props.BoolProperty(default = False)
    in_obj_mode : bpy.props.BoolProperty(default = False)
    observed_layers = dict()
    observed_objects = dict()
    non_objmode_journey = dict()
    #observed_frames = dict()

class GP_MASK_WINPROPS(PropertyGroup):
    bl_label = "gp mask window properties"
    bl_idname = "gpmask_winproperties"
    handler_on : bpy.props.BoolProperty(default = False)
    ob : bpy.props.PointerProperty(type = bpy.types.Object)
    gpm_i : bpy.props.IntProperty()

    last_ops = [None]
    
class GP_MASK_PROPS(PropertyGroup):
    
    bl_label = "gp mask properties"
    bl_idname = "gpmask_properties"

    src_ob : bpy.props.PointerProperty(type = bpy.types.Object)
    layers : bpy.props.CollectionProperty(type = GP_MASK_LAYER)
    is_realtime : bpy.props.BoolProperty(name= 'realtime calculation', default = False)
    invert : bpy.props.BoolProperty(name= 'invert mask', default = False)
    active_index : bpy.props.IntProperty(default = 0)
    skip_mode_update : bpy.props.BoolProperty(name= 'Skip mode update', default = False)
    mode : bpy.props.EnumProperty(name = 'GPM mode',items = [('OFF', 'OFF', 'disabled', 0),('BAKED', 'BAKED', 'BAKED', 1), ('ON', 'ON', 'enabled', 2) ], default = 'OFF', update = mode_update)
    #mode : bpy.props.EnumProperty(name = 'GPM mode',items = [('OFF', 'OFF', 'OFF', 0), ('REALTIME', 'REALTIME', 'REALTIME', 1), ('BAKED', 'BAKED', 'BAKED', 2)], default = 'OFF', update = mode_update)
    handler_on : bpy.props.BoolProperty(default = False)
    result_layer : bpy.props.StringProperty()
    result_layer_info : bpy.props.StringProperty(update = update_result_layer_info)
    offset : bpy.props.FloatProperty(default = 0, precision = 4)