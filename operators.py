import bpy 
from.gp_utils import *
from .ui import *
from .handlers import *
from .properties import *

class GP_MASK_OT_LAYER_ADD(bpy.types.Operator):
    bl_label = "add a new mask from object "
    bl_idname = 'gpm.layeradd'
    bl_options = {'REGISTER','UNDO'}       
    info : bpy.props.StringProperty()
    ob :  bpy.props.StringProperty()

    def execute(self, context): 
        gpm = context.object.gpmdatas[bpy.context.window_manager.gpm_window.gpm_i]
        new_layer = gpm.layers.add()
        new_layer.info = self.info
        new_layer.ob = bpy.data.objects[self.ob]
        observe_layer(context, new_layer, context.object, bpy.context.window_manager.gpm_window.gpm_i  )
        observe_object(context, context.object.gpmdatas[bpy.context.window_manager.gpm_window.gpm_i].src_ob, context.object.gpmdatas[bpy.context.window_manager.gpm_window.gpm_i])
        return{'FINISHED'}

class GP_MASK_OT_LAYER_REMOVE(bpy.types.Operator):
    bl_label = "add a new mask from object "
    bl_idname = 'gpm.layerremove'
    bl_options = {'REGISTER','UNDO'}    
    i : bpy.props.IntProperty()   

    def execute(self, context): 
        gpm = context.object.gpmdatas[self.i]
        unobserve_layer(context, gpm.layers[gpm.active_index], context.object, self.i )
        gpm.layers.remove(gpm.active_index)
        return{'FINISHED'}

class GP_MASK_OT_ADD(bpy.types.Operator):
    bl_label = "add a new mask from object "
    bl_idname = 'gpm.add'
    bl_options = {'REGISTER','UNDO'}       
    
    def execute(self, context): 
        ob = context.object
        ob.gpmdatas.add()  
        return{'FINISHED'}

class GP_MASK_OT_REMOVE(bpy.types.Operator):
    bl_label = "remove mask from object "
    bl_idname = 'gpm.remove'
    bl_options = {'REGISTER','UNDO'}     
    gpm : bpy.props.IntProperty()
    object_name : bpy.props.StringProperty()

    def execute(self, context): 
        ob = bpy.data.objects[self.object_name]
        if is_valid_gpm(ob.gpmdatas[self.gpm]):
            mask_layer = get_mask_layer(ob, ob.gpmdatas[self.gpm])
            if mask_layer :
                clear_layer_anim(mask_layer)
            remove_mask_layer(ob, self.gpm)
        for gpmlayer in ob.gpmdatas[self.gpm].layers :
            unobserve_layer(context, gpmlayer, ob, self.gpm)
            unobserve_object(context, ob.gpmdatas[self.gpm].src_ob, ob.gpmdatas[self.gpm])
        ob.gpmdatas.remove(self.gpm)       
        return{'FINISHED'}
    
class GP_MASK_OT_SWITCH_ALL_ON(bpy.types.Operator):
    bl_label = "switch on all gp mask mode"
    bl_idname = "gpm.switch_all_on"
    #bl_options = {'UNDO'} 
    def execute(self, context) :
        rebuild_observed_lists(context, context.scene)
        for ob in bpy.context.scene.objects :
            for gpm in ob .gpmdatas :
                if is_valid_gpm(gpm):
                    gpm.mode = 'ON'
        return {'FINISHED'}

class GP_MASK_OT_SWITCH_ALL_OFF(bpy.types.Operator):
    bl_label = "switch off all gp mask mode"
    bl_idname = "gpm.switch_all_off"
    #bl_options = {'UNDO'} 
    def execute(self, context) :
        for ob in bpy.context.scene.objects :
            for gpm in ob .gpmdatas :
                if gpm.src_ob :
                    gpm.mode = 'OFF'
        return {'FINISHED'}
    
class GP_MASK_OT_BAKE_ALL(bpy.types.Operator):
    bl_label = "switch on all gp mask mode"
    bl_idname = "gpm.bake_all"
    #bl_options = {'UNDO'} 
    
    def execute(self, context) :
        scene = context.scene
        rebuild_observed_lists(context, scene)

        masks = []
        for ob in bpy.context.scene.objects :
            for gpm in ob.gpmdatas :
                if is_valid_gpm(gpm):
                    masks.append(gpm)
        
        bake_all_masks(masks, scene.frame_start, scene.frame_end)

        for mask in masks :
            mask_layer = get_mask_layer(mask.id_data, mask)
            mask_layer.hide = False

            mask.skip_mode_update = True
            mask.mode = "BAKED"
            mask.skip_mode_update = False

        return {'FINISHED'}
    
class GP_MASK_OT_BAKE(bpy.types.Operator):
    bl_label = "bake gp mask "
    bl_idname = "gpm.bake"
    #bl_options = {'UNDO'} 
    i : bpy.props.IntProperty()
    object_name : bpy.props.StringProperty()
    
    def execute(self, context) :
        ob = bpy.data.objects[self.object_name]
        gpm = ob.gpmdatas[self.i]
        gpm.mode = 'BAKED'
        return {'FINISHED'}

class GP_MASK_OT_SWITCH_OFF(bpy.types.Operator):
    bl_label = "switch on gp mask mode"
    bl_idname = "gpm.switch_off"
    #bl_options = {'UNDO'} 
    i : bpy.props.IntProperty()
    object_name : bpy.props.StringProperty()
    
    def execute(self, context) :
        ob = bpy.data.objects[self.object_name]
        gpm = ob.gpmdatas[self.i]
        if gpm.mode != 'OFF' :
            gpm.mode = 'OFF'
        return {'FINISHED'}

class GP_MASK_OT_SWITCH_ON(bpy.types.Operator):
    bl_label = "switch on gp mask mode"
    bl_idname = "gpm.switch_on"
    #bl_options = {'UNDO'} 
    i : bpy.props.IntProperty()
    object_name : bpy.props.StringProperty()
    
    def execute(self, context) :
        ob = bpy.data.objects[self.object_name]
        gpm = ob.gpmdatas[self.i]
        gpm.mode = 'ON'
        if is_valid_gpm(gpm):
            observe_object(context,ob, None) 
            for gpmlayer in gpm.layers :
                observe_layer(context, gpmlayer, ob, self.i)     
                observe_object(context, gpm.src_ob, gpm)
        return {'FINISHED'}

class GP_MASK_OT_GENERATE(bpy.types.Operator):
    bl_label = "generate mask from object"
    bl_idname = "gpm.generate"
    #bl_options = {'UNDO'} 
    i : bpy.props.IntProperty()
    #refresh : bpy.props.BoolProperty(default = False)
    object_name : bpy.props.StringProperty()
    def execute(self, context):
        context.view_layer.update()
        if self.object_name != '' :
            object = bpy.data.objects[self.object_name]
        else :
            object = bpy.context.object
        generate(object, self.i, [])
        context.view_layer.update()
        return{'FINISHED'}

class GP_MASK_OT_MENU(bpy.types.Operator): #choose a layer in the menu list
    bl_label = "Layer Specials"
    bl_idname = "gpm.menu"
    i : bpy.props.IntProperty()
    def execute(self, context):
        context.window_manager.gpm_window.gpm_i = self.i
        context.window_manager.popup_menu(draw_menu)
        return{'FINISHED'}

class GP_MASK_RESTORE_DATA(bpy.types.Operator): #choose a layer in the menu list
    bl_label = "Repair gp_mask data"
    bl_idname = "gpm.repair_data"
    i : bpy.props.IntProperty()
    def execute(self, context):
        bpy.types.Scene.gpm_scene = bpy.props.PointerProperty(type = GP_MASK_SCENEPROPS )
        return{'FINISHED'}
    
 